package com.zellar.MainTest;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
* We have a list of customers and we are fairly certain
* that there are duplicates in the list.
*
* You must count the number of duplicates and possible duplicates.
*
* A duplicate is a customer where their first name, last name, and email address all match.
* A possible duplicate is where their first name and last name match but they have different email addresses.
*
* The list of customers is found in the json document 'customers.json' found in the resources directory.
* You must load and deserialize the document into a list of Customer objects. (The customer object is found in the models package)
* Feel free to use any open source library you would like to aid you in the deserialization. (Hint: Gradle is your friend)
* Once you have a list of customer objects count the number of duplicates and the number of possible duplicates.
* Print your totals on the console.
*
* Other Notes:
* A matching pair is considered 1 duplicate. Three of the same is considered two matches. (i.e. N - 1)
*
*/


/************************************************************************************************
* Must add json-simple-1.1.1.jar to external library for parser to work.
*/

public class CustomerDup {
	
	 public void findDuplicates() {
		
		List <Customer> custList = new ArrayList<>();
		List <Customer> custFullDupList = new ArrayList<>();
		List <Customer> custPossDupList = new ArrayList<>();
		JSONParser parser = new JSONParser();
		
	try {
				
			JSONArray a = (JSONArray) parser.parse(new FileReader("src/com/zellar/MainTest/customers.json"));
			for(Object obj: a)	{
				JSONObject customer = (JSONObject) obj;
				Customer cust = new Customer();
						
				cust.setFirstName((String) customer.get("firstName"));
				cust.setLastName((String) customer.get("lastName"));
				cust.setEmailAddress((String) customer.get("emailAddress"));
				
				custList.add(cust);	
			}
			List <Customer> custUniqueList = new ArrayList<>(custList);
			for(int i=0; i < custList.size(); i++){
				
				for(int j=1; j<custList.size(); j++){
					
					if(i!=j && custList.get(i).getLastName().equals(custList.get(j).getLastName())){
						if(i!=j && custList.get(i).getFirstName().equals(custList.get(j).getFirstName())){
							if(i!=j && custList.get(i).getEmailAddress().equals(custList.get(j).getEmailAddress())){
								custFullDupList.add(custList.get(j));
								custUniqueList.remove(custList.get(j));
							}else{
								custPossDupList.add(custList.get(j));
								custUniqueList.remove(custList.get(j));
							}
						}
					}
				}
			}
			
			System.out.println("Total Number of Customers "+custList.size());
			System.out.println("Number of Full Matches: "+custFullDupList.size());
			System.out.println("Number of Possible Matches: "+custPossDupList.size());
			System.out.println("Number of Unique Customers "+custUniqueList.size());
			        
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	 
  }

}
