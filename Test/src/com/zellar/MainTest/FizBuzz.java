package com.zellar.MainTest;

import java.util.List;

/**
 * Complete the method below.
 * This method will receive a list (as you can see below).
 * The method should iterate over the list.
 * For most indexes you should put the index value into the list at that index.
 * For indexes that are multiples of three, the value should be "Fizz".
 * For indexes that are multiples of five, the value should be "Buzz".
 * For indexes that are multiples of both three and five, the value should be "FizzBuzz".
 */

public class FizBuzz {

	public List<String> fizzBuzz(List<String> fizBuzz){
    	
    	String temp = null; //String of the index number to add to the list if not divisible by 3 or 5
		for(int i=0; i<fizBuzz.size(); i++){
			
			temp = Integer.toString(i);
			if(i==0){
				fizBuzz.set(i,temp);
			}else
			if(i%3 == 0 && i%5 == 0){
				fizBuzz.set(i,"fizBuzz");
			}else
			if(i%3 == 0){
				fizBuzz.set(i,"fiz");
			}else
			if(i%5 == 0){
				fizBuzz.set(i,"Buzz");
			}else{
				fizBuzz.set(i,temp);
			}
		}
		
        return fizBuzz;
    }
}
