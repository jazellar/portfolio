package com.zellar.MainTest;


import java.io.CharArrayReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/***
		 * For problem 1 -- FizBuzz
		 */
		List<String> fizzBuzz = new ArrayList<>();
		List<String> fizzBuzzList = new ArrayList<>();
        IntStream.range(0,100).forEach(item -> fizzBuzz.add(""));
        FizBuzz fizBuzz = new FizBuzz();
        fizzBuzzList = fizBuzz.fizzBuzz(fizzBuzz);
        System.out.println("FizBuzz Problem:");
        for(String tmp: fizzBuzzList){
        	System.out.println(tmp);
        }
        System.out.println("************");
        
        /****
         * For problem 2 -- Palindrome
         */
        Palindrome pal = new Palindrome();
        String str = null;
        str = Boolean.toString(pal.isPalindrome("RACECAR"));
        System.out.println("Palindrome Problem:");
        System.out.println(str);
        System.out.println("************");

        /****
         * For Problem 3 -- Customer Dups
         */
        System.out.println("Customer Dup Problem:");
		CustomerDup custDup = new CustomerDup();
		custDup.findDuplicates();
		
	}
}
