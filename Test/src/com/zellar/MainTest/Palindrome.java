package com.zellar.MainTest;

public class Palindrome {

	    public boolean isPalindrome(String value){
	    	Boolean isPal = false;//flag for if two side match
			char charArray[] = value.toCharArray();
			int j = charArray.length-1;
			/*****
			 * For loop iterate both ends of the string.
			 * Once the two side don't match -- for loop breaks
			 * *****/
			for(int i=0; i<charArray.length;i++){
				char left = charArray[i];
				char right = charArray[j];
				if(i!=j){
					if(left == right){
						isPal = true;
					}else{
						isPal = false;
						break;
					}
				}		
				j--;
			}
			if(isPal){
				return true;
			}else{
				return false;
			}
	    }
	}


