 /*********
  *author: Jeff Zellar
  *class: CS-431
  *due date: 2/7/13
  *
  *The purpose of this program is to convert rods into yards.
  *First the user will be presented with a list of directions.  Then the 
  *function inpputCheck() is called.  Here the user is prompted to 
  *enter a distance in rods and press enter.  The function accepts the value
  *as a string and runs a check to compare each char with a list "123456789.-".
  *If a char doesn't match it prints a message "Invalid Number!" and displays the
  *char.  If valid, the function converts the string to a float and returns is to
  *main. 
  *Then the program will call the converter() function and convert the number into 
  *yards and return that value.
  *This process with repeat until the user enters a negative number
  *for the distance in rods.  Then the program will terminate.
  *
  *******Variables*******
  *  r == value that user enters -- in rods
  *  c == value of constant to multiply the rods by
  *  y == converted value -- in yards
  ***********************
  *
  **********/
#include <iostream>
#include <string>
#include <cstdlib> //for atof

float inputCheck();//function to check if input if valid
float converter(float r);//function to convert rods to yards
 
int main(){
float r = 0;
float y = 0;

// instructions to the user
std::cout << "\n***Instructions on how to use this rods-to-yards convertion program.***" << std::endl;
std::cout << "1) Enter the distance in rods. (Entering a negative number will terminate the program.)\n"
    << "2) Press Enter.\n"
    << "3) The program will then give you the new distance in yards.\n"
    << "4) This process will continue until you enter a negative number.\n";
 
// input in rods
do{
    r = inputCheck();
    if (r>=0){//check to see if r is a non-negative number (meaning user wants the number to be converted from rods to yards).
	y = converter(r);
	
	std::cout << "Distance in yards: " << y << std::endl;
	}
    }while (r>=0);//program continues until user enters a negative number (meaning they are finished and want to exit the program.

return 0;
}

/*********
 * Function: inputCheck()
 * This function is used to check if the input that the user gives is
 * a valid float.  
 * I do this by accepting the input as a string and then compare each
 * char to a list.  If not in list -> then error message.  This is
 * done using the std::string function "find_first_not_of()".  Otherwise
 * input is good and converts it from string to float and returns
 * value to main().
 *********/

float inputCheck(){
    std::string num1;
    float x = 0;

    std::cout << "\nEnter the distance in rods." << std::endl;
    std::cin >> num1;
    
    if(num1.find_first_not_of("1234567890.-") != std::string::npos){
	std::cout << "Invalid Number! " << num1 << std::endl;
	num1 = -1;
    }
x = atof(num1.c_str());
return x;
}

/*********
 * Function: converter()
 * This function takes the distance that the user gives (assumes distance is in rods).
 * Then converts it to yards by multiplying it by 5.5.
 *********/

float converter(float r){
    // equation to convert rods into yards is rod * 5.5 = yards
    
    float y; //holds the value of yards
    float c = 5.5; //convertion value
    y = r * c;
    return y;
    }


