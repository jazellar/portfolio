/************
 * Author: Jeff Zellar
 * CS-431: Program 1 -- Displaying System Information from a Linux OS
 * Due: 2/21/13  (Extended to Sunday 2/24/13)
 *
 * The objective of this program is to access specific files within
 * the Linux kernel and aquire 5 pieces of information:
 * 1) Machine name (hostname) -- found in /proc/sys/kernel directory
 * 2) System date and time -- I'm using a "time" function taken from a
 * 	class example, as per the requirements.
 * 3) Linux kernel version -- found in /proc/version directory
 * 4) Memory usage (used and free memory) -- found in /proc/meminfo
 * 5) Identify number of CPU's on the host system -- found in
 * 	/proc/cpuinfo.
 * Note: For #5 the requirements state to only count and report on the
 * number of CPU's present.  To do that the program must read the data
 * in the file and only count everytime a new CPU is identified.
 *
 * Once all this information is gathered, then the program is to
 * display the info on the screen in a neat readable format.
 *
 * ****Variables****
 * hostName -- string to hold machine name
 * now -- holds the current time
 * version -- an array the holds the version of Linux currently
 * 	      running
 * usage -- an array the holds memory info, used and free.
 * count -- int that holds the number of CPU's
 *
 * */


#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

/*** Function declarations ***/

std::string getHostName(); //Function to get Machine Name
void getVersion(std::string version[]); //Function to get Linux Kernel Version
void getMemoryUsage(int usage[]); //Function to get info on memory -- used and free memory
int getcpuCount(); //Function tells how many CPU's there are on system
void printInfo(std::string hostName, time_t now, std::string version[], int usage[], int count); //Function to print out all system info

int main(){

/*** Retrieving Machine Name ***/
    std::string hostName;
    hostName = getHostName(); 

/*** Retrieve System Date and Time ***/
    time_t now;
    time (&now);

/*** Retrieve Kernel Version ***/
    std::string version[3];
    getVersion(version);

/*** Retrieve Memory Usage ***/
    int usage[2];
    getMemoryUsage(usage); 

/*** Retrieve Number of CPU's on Current System ***/
   int count;
   count = getcpuCount();

/*** Final print out of system info ***/
    printInfo(hostName, now, version, usage, count);

    return 0;
}

std::string getHostName(){

    std::string host;
    std::ifstream hostFile;
    hostFile.open ("/proc/sys/kernel/hostname");
    if(!hostFile.good()){
       std::cout << "*******\nUnable to open hostname file!\n*******\n";
       exit(1);
    }
    hostFile >> host;
    //close file
    hostFile.close();
    
    return host;

}

void getVersion(std::string version[]){

//    I chose to make this an array inorder to utilize the info in the
//    file and minimize that amount of "hard coding" I would have to
//    do in the print function
    std::ifstream verFile;
    verFile.open ("/proc/version");
    if(!verFile.good()){
	std::cout << "*******\nUnable to open version file!\n*******\n";
	exit(1);
    }
    verFile >> version[0] >> version[1] >> version[2];
    //close file
    verFile.close();
}

void getMemoryUsage(int usage[]){

    int free = 0;
    int total = 0;
    int used = 0;
    std::string temp; // used to step through file to get to integer needed 
    std::ifstream memFile;
    memFile.open ("/proc/meminfo");
    if(!memFile.good()){
        std::cout << "*******\nUnable to open memory info file!\n*******\n";
        exit(1);
    }
    memFile >> temp/*MemTotal*/ >> total >> temp/*kb*/ >> temp/*MemFree*/  >> free;
    //close file
    memFile.close();
    used = total - free;
    usage[0] = used;
    usage[1] = free;
}

int getcpuCount(){

    int count = 0;
    std::string temp;
    std::ifstream cpuFile;
    cpuFile.open ("/proc/cpuinfo");
    if(!cpuFile.good()){
        std::cout << "*******\nUnable to open cpu info file!\n*******\n";
        exit(1);
    }
    while(!cpuFile.eof()){
	cpuFile >> temp;
	if(temp == "processor")
	    count++;    
    }
    //close file
    cpuFile.close();

    return count;
}

void printInfo(std::string hostName, time_t now, std::string version[], int usage[], int count){

    std::cout << std::endl << "Machine Name for this system is:\n" << hostName << std::endl;
    std::cout << std::endl << "Date and Time on this system is:\n" << ctime(&now);
    std::cout << std::endl << "Version of Linux running on this system is:\n" 
	<< version[0] << " " << version[1] << " " << version[2] << std::endl;
    std::cout << std::endl << "The Memory Usage for this system is --\nUsed: " << usage[0] << std::endl
	<< "Free: " << usage[1] << std::endl;
    std::cout << std::endl << "The Number of CPU's on this system is:\n" << count <<  std::endl << std::endl;

}
