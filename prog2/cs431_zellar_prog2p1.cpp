/***********************
 * file name: cs431_zellar_prog2p1.cpp
 * Author: Jeff Zellar
 * Due Date: 3/12/13
 *
 * The purpose of this program is to read in commands from 
 * a file and execute them within this progam
 * using fork() function.
 * ********************/
#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
extern char **environ;
extern int errno;
pid_t pid;

int main(int argc, char *argv[]){
    if(argc != 2){
	std::cout << "Error -- program needs input file.\n";
	std::cout << "Usage: " << argv[0] << " <filename>\n";
    }

/****Variables to manage input command****/
    std::string cmd = "";
    char *command[]= {NULL};
    int status;
    int round = 1;

    std::ifstream fin(argv[1]);
    if(fin.is_open()){
	std::cout << "\n********** Let the Games Begin **********\n";

/****Loop that continues until end of file****/
	while(!fin.eof()){
	std::cout << "\n\n********** " << round++ << " **********\n\n";
    	std::getline(fin, cmd);
	if(cmd.empty()) //Checks if cmd is empty -- meaning nothing was read in.
	    break;
	pid = fork(); //Creates a child process -- returns a value of 0.

	if(pid<0){ // fork() should return 0 if it worked properly
	    std::cout << "Unable to fork!\n";
	    continue;
	}

	if(pid > 0){ //If pid is greater than 0 than we are in the parent process 
	    waitpid(-1, &status, 0); //waits until child process is finished to continue
	}
	
	if(pid==0){
	    std::cout << "We are in the child now." << std::endl;

	    /**********************************
	     * This segment of code is used to convert the 
	     * string that was read from the file to 
	     * a char *const array -- which is what 
	     * execve() needs as its second argument.
	     * ******************************/

    		int strLen=0;
		strLen = cmd.length();
		char *const myArray = new char[strLen+1];
		for(int i=0; i<strLen; i++){
		    myArray[i] = cmd[i];
		}//end for
		char *const args[] = {myArray, (char *) 0};

	    /*********************************/
	
	    std::cout << "Command to be executed in this round is: " << args[0] << std::endl;
	    char *path[2];
	    path[0]= myArray;
	    if(execve(path[0], args, NULL) == -1){ //if execve() returns -1 than it failed
		std::cout << "ERROR IN EXECVE" << std::endl;
		perror("execv");
	    }//endif
	    delete [] myArray; //freeing up memory 
	}//endif--(pid==0)
	else{
	    std::cout << "Created new process with process ID: " << pid << std::endl;
	}
	} //endwhile
    } //endif--(fin.open()) 
    std::cout << "Program Complete!" << std::endl; //Shows that the program finished successfully
    fin.close();
return 0;
} //main

