
/******************************
 * File: cs431_zellar_prog2p2.cpp -- part B of program 2
 * Author: Jeff Zellar
 * Due Date: 3/12/13
 *
 * The purpose of this program is to work with threads.
 * The program will have two arguments passed to it
 * 1) # of threads to create
 * 2) time interval that each thread will run before 
 * 	terminating.
 * ***************************/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <pthread.h>
#include <sstream>
#include <math.h>

//The code executed by each worker thread (simulated work...) 
void* work(void * arg);

//Data to be passed into thread.  Using struct in order to passed
//three variables -- 1)threadId 2)time thread should run 3)time
//interval.
struct tdata{
    pthread_t id;
    int tnum;
    int time;
};
                                                                                                       
int main(int argc, char *argv[]){
    if(argc != 3){ // program name and two arguments
	std::cout << "ERROR -- program needs 2 numbers with it.\n";
	std::cout << "First one is the number of threads to create.\n";
	std::cout << "Second one is the time interval each thread will run.\n";
    }

/*** Converting argv[1], and argv[2] into int. ***/
    int numThread;
    int timeInterval;
    std::istringstream issN(argv[1]);
    issN >> numThread;
    std::istringstream issT(argv[2]);
    issT >> timeInterval;

    time_t now;
    time (&now);
    pthread_t thread[numThread];
    int status;
    tdata data[numThread];

    /******** Header for output *************/
    std::cout << "\n********************************* START **************************\n";
    std::cout << "Number of threads: " << numThread << std::endl;
    std::cout << "Runtime (sec): " << timeInterval << std::endl;
    std::cout << "Current time and date: " << ctime(&now) << std::endl << std::endl;

    // For Loop to create threads
    for(int i=0; i < numThread; i++){
    	data[i].time = timeInterval;
    	data[i].tnum = i;
	status = pthread_create(&thread[i], NULL, &work, (void *)&data[i]);
	sleep(1);
	if(status != 0){//Checks if pthread_create is successful
	    std::cout << "ERROR. pthread_create returned error code " << status << std::endl;
	    exit(-1);
	}
    }

    /*************** Variables and Loop to allow main 
     * 		     process to wait a specified length of time.******************/
     
    time_t begin = time(&begin);
    time_t end = time(&end);
    long L = end - begin;
    std::cout << "\n********************************************************\n";
    std::cout << "Now waiting for threads to finish:\n";
    std::cout << "\n********************************************************\n";
    while(L < timeInterval){
	std::cout << "Waiting " << L << " second.\n";
	sleep(1);
	end = time(&end);
	L = end - begin;
    }
    
    std::cout << "********************** END ************************************\n";
    std::cout << "All Threads have finished.  Program terminating.\n";
    
    return 0;
}

/***** Function that does some work *****/
void* work(void * arg){
    double y;
    double x = 3.0; 
    double e = 2.0; 
    int i,j; 
    time_t now;
    time (&now);
    struct tdata * myData;
    myData = (struct tdata*) arg;
    myData->id = pthread_self();
    std::cout << "Thread " << myData->tnum << " with ID: " << myData->id << " was created at " << ctime (&now) << std::endl;
//Set X large enough to ensure sufficient runtime 
    for (i = 0; i < 100000; i++){
	for (j = 0; j < 400000; j++){
	    y = pow(x,e);
	}
	printf("Loop %d of thread %d work cycle\n", i, myData->tnum);
/*****
* This sleep() command is only here for readablility in final output.
* Normal processing would not have this here.
* ***/
	sleep (1);
    }
} 
