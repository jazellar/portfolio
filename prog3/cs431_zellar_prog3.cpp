/******************
 * Author: Jeff Zellar
 * Due Date: 4/4/13
 *
 * The purpose of this program is to work more with multiple
 * processes. The user must pass in three arguments to the program:
 * 1) the program to execute to calculate prime numbers
 * 2) the number used to determine which prime number (after
 * calculations) the user is looking for. (the nth prime)
 * 3) the number of processes to be created to run prime
 * calculator on.
 * *****************/

#include <iostream>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sstream>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>


int main(int argc, char* argv[]){
    if(argc != 4){
	std::cout << "Error -- program requires 3 arguments.\n";
	std::cout << "./prog3, <which prime # to find>, <# of processes to create>\n";
    }
    
    /***********
     * this section is for converting and setting up 
     * all arg[] for execve() function
     * ********/
    char* progName = argv[1];
    strcpy(progName, argv[1]);
    int primeNum, procNum;
    std::istringstream issN(argv[2]);
    issN >> primeNum;
    std::istringstream issP(argv[3]);
    issP >> procNum;
    int status;
    pid_t pid;

 /*****
  * set up time tracker using gettimeofday()
  * ***/
    timeval tim; 
    gettimeofday(&tim, NULL);
    double start = tim.tv_sec+(tim.tv_usec/1000000.0);


    for(int i=0; i<procNum; i++){ //Loops to create user defined number of processes
	pid = fork(); //Creates a child process -- returns a value of 0.
	
	if(pid<0){ // fork() should return 0 if it worked properly
	    std::cout << "Unable to fork.\n";
	    continue;
	}

	if(pid==0){

	 /*********************************
	  * This section of code is 
	  * used to convert the for-loop
	  * variable i from an int to a char* 
	  * in order to be passed as the second 
	  * argument of the execve() function.
	  * ******************************/
	    std::stringstream ss;
	    ss << i;
	    std::string s(ss.str());
	    std::cout << s << std::endl;
	    char* const pn = new char [s.length()+1]; // Converts int to char*
	    strcpy (pn, s.c_str());
  	 /************************************/

	 /***** Array of arguments to use with exec() *****/
	    char* args[4]; //array of arguments for execve()
	    args[0] = progName; //program to be executed
	    args[1] = argv[2]; //the nth prime number program 2 must find
	    args[2] = pn; //process number to set range with
	    args[3] = (char*) 0; //array must be NULL terminated

	    if(execve(argv[1],args,NULL) == -1){
		 std::cout << "ERROR IN EXECVE" << std::endl;
		 perror("execv");
	    }
	    delete [] pn;
	}else{
	    std::cout << "\nJust created process: " << pid << std::endl;
	}
    }
    
    waitpid(pid, &status, 0); //waits until child processes are finished

    /******
     * finish time elapse calculations
     * ***/
    gettimeofday(&tim, NULL);
    double end = tim.tv_sec+(tim.tv_usec/1000000.0);
    std::cout << "\nElapse time is: " << end-start << std::endl; //prints time elapse
	    
    std::cout << "Program Complete!" << std::endl; //Shows that the program finished successfully

    return 0;
}//end main
