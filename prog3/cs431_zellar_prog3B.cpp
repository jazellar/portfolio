/******************
 * Author: Jeff Zellar
 * Due Date: 4/4/13
 *
 * The purpose of this program is to calculate all the prime numbers
 * up to a specified.
 * This program will be executed by another one and will be given 2
 * arguments
 * 1) Process #
 * 2) the nth prime number to return to main program.
 * ***************/

#include <iostream>
#include <sstream>
#include <cmath>

void prime_num(int, int, int, int);

int main(int argc, char* argv[]){
    if(argc != 3){
	std::cout << "Error -- program requires 2 arguments.\n";
	std::cout << "<which prime # to find> "
	    "<which process we are in>\n";
    } 

 /************* 
  * This section is used to convert the input
  * arg[] into int primeNum and int procNum
  * ***********/
    int primeNum, procNum;
    std::istringstream issN(argv[1]);
    issN >> primeNum;
    std::istringstream issP(argv[2]);
    issP >> procNum;

    procNum++; //makes process number more readable. (ex: first process is 1)
    std::cout << "nth prime number is: " << primeNum << std::endl;
    std::cout << "Process number is: " << procNum << std::endl;

    int min, max;
	min = procNum * 10000000;
	max = (procNum + 1) * 10000000;

    prime_num(min, max, primeNum, procNum);
    std::cout << "\nProcess " << procNum << " just terminated.\n";

    return 0;
}

void prime_num(int min, int max, int num, int proc){
    std::cout << "Min: " << min << std::endl << "Max: " << max << std::endl;
    int pnum = 0;
    int count = 0;
    bool isPrime = true;
    
    int num2 = (int) floor (sqrt (max)); //gives base numbers to check if i is divisible by j
    for(int i = min; i <= max; i++){
	isPrime = true;
	if(i%2 == 0){ //checks for even numbers
	    isPrime = false;
	}//endif

	for(int j = 2; j < num2; j++){
	    if(i !=j && i % j == 0){
		isPrime = false;
		break;
	    }//endif
	}//endfor
	
	// if prime the increase count and pnum. also prints process
	// number for every 1000th count (used for testing to see
	// progress.  will comment out for final submitting)
	if(isPrime){ 
	       count++;
	       pnum++;
	    if(count == 1000){
		std::cout << proc << "  ";
		count = 0;
	    }//endif
	}//endif	   
	
	// when pnum == num the the search is over.
	if(pnum == num){
	    std::cout << "\nThe " << num << "th prime number for process " << proc << " is: " << i << std::endl;
	    return;
	}//endif
    }//end i for loop
}//end prime_num()
