/*****************************
 * Author: :Jeff Zellar
 * Due Date: 4/22/13
 * 
 * The purpose of this program is to work with threads and synchronisation.
 * To do that the program will simulate ticket sale to a stadium
 * event that has 1,000,000 seats.  Tickets will be sold via "sales
 * agents" (threads).  The user will pass in, via command line args,
 * the number of agents for the simulation (between 15 - 30).
 * Each agent will be assigned an ID number to track sales.  The agent
 * with the most sales wins a prize.  
 * Agent Limits and Requirements:
 * 	-Assigned Agent ID number must start at 1.
 * 	-Agent can not sell more than 10 tickets at one time.
 * 		--A random number between 1 - 10 will be generated
 * 		each time an agent sells tickets.
 * 	-Agent must sell seats in a consecutive block.
 * 		--i.e. Agent 1 sells seats 20-25.  Agent 2 must start
 * 		sales at 26.
 * 	-At the end of each sale, agent must do some "busy" work in
 * 	order to slow the whole process down and cause other agents to
 * 	wait.  This is where the syncronisation could breakdown.
 * Seat availability will be tracked using an array[1,000,000] (commas
 * for clearity) with value of zero meaning seat is available and if sold 
 * will be replaced with agent number.
 *
 *
 ******************************/

#include <iostream>


void busyWork();
int main(int argc, char* argv[]){

    if(argc != 2){
	std::cout << "Error -- program requires 1 argument.\n";
	std::cout << "./prog4 <# of agents>\n";
    }


    busyWork();

   return 0;
}

void busyWork(){

}
